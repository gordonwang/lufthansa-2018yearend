<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
  <title>奥地利航空AUSTRIAN</title>
  <link href="{{asset('public/web/style/commons.css')}}" rel="stylesheet">
  @yield('head')
</head>
<!-- 导入Parllay的tracking JS文件 -->
<script src="http://cdn.parllay.cn/libs/tracking.js"></script>
<script>
    fht('create', '{{config("app.track_set_id")}}', 'http://tracking.parllay.cn');
    fa('add',{
        fht_open_id: '{{$user_open_id}}', //用户的openID，请用实际值替换
    });
    //pageView事件 , 第3个参数为变量，可以使用例如 location.href.split(‘#')[0];
    //注意第3个参数必须要用JS的 encodeURIComponent() 函数处理
    fa('send','fht_page_view', encodeURIComponent(location.href.split('#')[0]));
    //其它页面的fa()函数仍然可以使用之前的代码
</script>
<body>
<div class="base-tips" data-item="tips"></div>
<div class="base-container">
    @yield('content')
</div>
<script type="text/javascript" src="{{asset('public/newWeb/entries/commons.js')}}"></script>
@yield('script')
@if(is_wechat())
  <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    var WX_APPID = "{{ config('app.wx_app_id') }}";
  </script>
  <script src="{{ asset('public/js/wechat.js') }}"></script>
  <script type="text/javascript">
    /* 自主生成wx.config */
    var js_ticket = "{{ $js_ticket }}";
    wxconfigSet(js_ticket);
  </script>
@endif
</body>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113308626-11"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-113308626-11');
</script>
</html>
