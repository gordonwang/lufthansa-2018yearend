<!doctype html>
<html lang="zh-CN">
<meta name="viewport" content="width=device-width, initial-scale=1; user-scalable=no">
<meta name="Content-Type" content="text/html;charset=utf-8"/>
<meta charset="utf-8"/>
<link rel="stylesheet" href="{{asset('public/web/css/main.css')}}">
<script src="{{asset('public/web/js/jquery-3.3.1.min.js')}}"></script>
<!--<script src="assets/js/app.js"></script>-->
<script src="{{asset('public/web/dist/js/main.bundle.js')}}"></script>
<script src="{{asset('public/web/js/vconsole.min.js')}}"> </script>

<!-- 导入Parllay的tracking JS文件 -->
<script src="http://cdn.parllay.cn/libs/tracking.js"></script>
<script>
    fht('create', '{{config("app.track_set_id")}}', 'http://tracking.parllay.cn');
    fa('add',{
        fht_open_id: '{{$user_open_id}}', //用户的openID，请用实际值替换
    });
    //pageView事件 , 第3个参数为变量，可以使用例如 location.href.split(‘#')[0];
    //注意第3个参数必须要用JS的 encodeURIComponent() 函数处理
    fa('send','fht_page_view', encodeURIComponent(location.href.split('#')[0]));
    //其它页面的fa()函数仍然可以使用之前的代码
</script>

<body>
<div class="step-01 container">
    <div class="fullscreen">
        <img src="{{asset('public/web/images/step_01_bg.jpg')}}"/>
    </div>
    <div class="logo"><img src="{{asset('public/web/images/logo.png')}}"/></div>
    <div class="content-box">
        <div class="title">
            <img src="{{asset('public/web/images/step_01_title.png')}}"/>
        </div>

        <div class="rulebutton">
            <img class="checkbox" src="{{asset('public/web/images/step_01_rule_checkbox_0.png')}}"/>
            <img class="agreebtn" src="{{asset('public/web/images/step_01_rule_agree.png')}}"/>
            <img class="rule1" src="{{asset('public/web/images/step_01_rule_1.png')}}"/>
            <img class="rule2" onclick="gtag('event', 'Button', { 'event_category': 'Homepage', 'event_label': 'Rules' });" src="{{asset('public/web/images/step_01_rule_2.png')}}"/>
        </div>

        <div class="buttons">
            <div onclick="gtag('event', 'Button', { 'event_category': 'Homepage', 'event_label': 'Start' })">
                <img class="leftbtn" src="{{asset('public/web/images/step_01_button_01.png')}}"/>
            </div>
            <div onclick="gtag('event', 'Button', { 'event_category': 'Homepage', 'event_label': 'Review' });">
                <img class="rightbtn" src="{{asset('public/web/images/step_01_button_02.png')}}"/>
            </div>
        </div>
        <!--<div class="rulebutton" onclick="gtag('event', 'Button', { 'event_category': 'Homepage', 'event_label': 'Rules' });">
            <img src="{{asset('public/web/images/step_01_rule_button.png')}}"/>
        </div>-->
        <div class="rule-mask-box" style="display: none;">
            <img class="bg" src="{{asset('public/web/images/step_01_rule_mask.png')}}" />
            <img class="button" src="{{asset('public/web/images/step_01_rule_make_ok_button.png')}}" />
        </div>
    </div>
</div>
<div class="step-02 container">
    <div class="fullscreen">
        <img src="{{asset('public/web/images/step_01_bg.jpg')}}"/>
    </div>
    <div class="logo"><img src="{{asset('public/web/images/logo.png')}}"/></div>
    <div class="content-box">

        <div class="content rule1">
            <div class="header">
                隐私政策
                <img class="closebtn" src="{{asset('public/web/images/closebtn.png')}}" />
            </div>
            <div class="body">



                <p>德国汉莎航空公司上海办事处 是一家根据中国法律组建和登记注册的公司，公司注册登记号为91310000X07210350K（在本隐私政策中称为&ldquo;我们&rdquo;）。我们承诺尊重您的隐私并遵守适用法律（定义见下文）以及恪守汉莎航空集团内部的数据保护指导准则，以确保您提供给我们的个人信息得到适当的保护以及公平合法的处理。&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>
                <p>您的个人信息将由我们持有和管理。您通过访问和使用我们不时提供的网站或移动网站（合称&ldquo;<strong>网站</strong>&rdquo;）或任何应用程序（&ldquo;<strong>应用程序</strong>&rdquo;），或者通过合约交易（如预订机票）或体验产品或与我们的内容和/或服务互动，或者通过联系我们的客户服务团队，您确认您已阅读本隐私政策。您必须年满16周岁才能使用我们的网站和应用程序。如果您未满16周岁或者在您的国家或居住地属于未成年人，请让您的父母或法定监护人为您提供他们的信息。&nbsp;</p>
                <p>本隐私政策涵盖我们收集、储存、使用和以其他方式处理的与您作为我们的客户或潜在客户相关的所有个人信息（其中也包括我们从腾讯获得或导出的获得信息）。我们将按照本隐私政策的规定且在适用法律允许的情形下收集、储存和使用您的个人信息（&ldquo;<strong>许可用途</strong>&rdquo;），上述情形包括：（i）因提供或履行您所要求的服务或他人要求我们为您提供的服务而必须收集和使用您的个人信息；（ii）您已向我们表示明确同意；或（iii）我们为寻求下列一项或多项合法利益而必须收集、储存和使用您的个人信息：</p>
                <ul>
                    <li>向客户提供产品或服务，或客户可能感兴趣的内容。&nbsp;</li>
                </ul>
                <ul>
                    <li>分析客户模式以及更好地了解我们的主要客户的需求和大众偏向，以便我们能够持续改进我们的产品和服务。&nbsp;</li>
                </ul>
                <ul>
                    <li>增强客户体验和服务，实现方式包括确保对与产品、服务和活动相关的网站和应用程序进行定制化设置，使其更客户的具体偏好。</li>
                </ul>
                <ul>
                    <li>确保我们的服务、网站和应用程序正常运行且安全，例如通过防止诈骗交易。</li>
                </ul>
                <ul>
                    <li>出于内部报告目的分析汉莎航空网站和汉莎航空推广活动或其它活动中的客流，并确保提供充足适当的产品和资源。</li>
                </ul>
                <ul>
                    <li>分析客户数据以便更好地了解、分析和监控客户模式和需求，从而持续改进其的产品和服务并了解您和其他客户可能感兴趣的其他产品和服务。</li>
                </ul>
                <ul>
                    <li>确保我们保持遵守适用法律，例如，使我们的记录保持准确的同时遵守适用法律。</li>
                </ul>
                <p>&nbsp;</p>
                <p><strong>我们收集的个人信息及其储存和使用</strong></p>
                <p>当您向我们（无论通过网站或应用程序或网站内发布及应用程序内发布的推广内容）注册帐户、下单、交易、参与推广活动（如参与调查或参赛或任何其他活动等）、在您的计算机或移动设备上与我们的数字内容进行互动、请求提供服务或向我们发送咨询请求时，我们将收集您的某些信息。届时我们会说明所收集的信息是否涉及个人信息，如涉及，那么是由您自愿提供还是您为了获得服务须按强制性要求提供。届时我们会说明所收集的信息是否涉及个人信息，如涉及，那么是由您自愿提供还是您为了获得服务须按强制性要求提供。请注意，如果收集个人信息是为了遵守强制性要求，若您不向我们提供这些信息，我们可能就无法向您提供相关产品或服务。请参阅本隐私政策的各项条款，以便了解更多关于我们收集哪些个人信息以及如何使用的规定。</p>
                <p>&nbsp;</p>
                <p><strong>账户信息</strong></p>
                <p>我们可收集以下信息：您在我们基于所述目的发布的应用程序上您所注册的账户信息，包括但不限于微信账号，微信昵称，头像图片，性别，国家，省份和城市，以及通过您访问网站所注册的账户信息，包括但不限于：您的姓名、电话号码、手机号码、电子邮件地址、目的地、最近机场和出行目的等信息<em>。</em>我们使用这些信息来识别您的客户身份、处理您的订单、交付产品和服务、处理付款、更新我们的记录以及综合管理您根据与我们之间的条款向我们开设的账户。向我们提供您的信息(包括个人信息)是基于您的自愿。</p>
                <p>&nbsp;</p>
                <p>如果适用法律允许或者（在适用法律要求的情况下）经您同意后，我们可使用有关您如何使用网站、应用程序和任何汉莎航空服务的信息，对出现在网站和/或应用程序上的内容及其呈现方式，从而使我们的网站、应用程序以及服务和通信与您更加密切相关，并使您能够使用我们服务的任何互动功能。</p>
                <p>&nbsp;</p>
                <p>如果您有客户账户（无论通过网站或应用程序或网站内发布及应用程序内发布的推广内容创建的），您确认，我们也可收集关于您在网上浏览或购买的产品或产品购买地、服务或接受服务地的信息以及与您的购物或接受服务有关的或者与我们的客户关系有关的其他信息。我们使用您的客户账户和个人资料信息，根据您的偏好，为您提供增强型服务，包括通过了解和确定您可能感兴趣的相关产品、服务和活动，为您提供个性化的体验，并与您共享关于您以往购物或接受服务历史的信息。&nbsp;</p>
                <p>&nbsp;</p>
                <p>我们还可将您的个人信息连同非个人信息一起用于对我们的网站流量分析、内部市场分析，以便分析、描述和监控客户模式，使得我们能够持续改进我们的产品和服务并了解您和其他客户可能感兴趣的内容，以便提供更多个性化和综合性的购物和互动体验。</p>
                <p>&nbsp;</p>
                <p>如果您不希望我们以这种方式使用您的个人信息，您可以随时联系我们，要求我们停止使用（请参阅下文&ldquo;联系汉莎航空&rdquo;）。</p>
                <p>&nbsp;</p>
                <p><strong>设备信息</strong></p>
                <p>我们收集您用于连接到汉莎航空的网站、应用程序上的服务或我们可能提供的任何交互式内容的设备的信息。这些信息包括您使用的设备类型、您的互联网浏览器、根据您设备的唯一标识符（如互联网协议（IP）地址或在您的设备上运行的应用程序的代码）确定的您的位置。&nbsp;</p>
                <p>&nbsp;</p>
                <p>我们使用这些信息来以确保我们的网站和应用程序运行正常和安全，我们优化提供给您的服务，并更好地了解其内容和服务被使用的方式及其效果。借助这些技术通过您的移动设备在网站收集的信息也为我们提供了关于我们网站客流量的汇总和匿名统计数据以及我们访客在性别、身高、体重、种族和原籍国方面的大致分布信息。</p>
                <p>&nbsp;</p>
                <p><strong>小文本文件</strong></p>
                <p>Cookies是在您访问网站或某些应用程序时向您的设备发送的且存储并有时跟踪记录您对该网站或应用程序的使用情况的一小段信息。当您进入我们的应用程序时，相关的网络服务器向您的设备发送一个cookie，这个cookie使我们能够识别您的设备。通过将cookies中的识别号码与您在登录网站或应用程序时提供的其他客户信息相联系，我们就能了解与您相关的cookie信息。</p>
                <p>&nbsp;</p>
                <p>我们通过使用cookies、本地存储和其他可能在您的设备上放置代码或从您的设备中获取信息和/或获取您设备信息的类似技术，提升您的用户体验以及汉莎航空的网站、应用程序和服务的质量。这包括记录您的购物记录，在您回到汉莎航空的网站时还能记得您，确认您在互联网上的浏览路径，此等使用使我们得以为您提供定制化服务，和/或让您免于再次输入相同信息之劳。我们还可使用cookies和其他技术了解我们网站各部分的使用情况，也可与通过使用cookies帮助我们在您访问的其他网站上展示与您相关的广告和使我们能够评估前述广告的效果的第三方广告网络合作。</p>
                <p>&nbsp;</p>
                <p>大多数网络浏览器的初始设置均接受cookies。您可以重新设置您的网络浏览器，拒绝所有cookies或指示何时发送cookies，但如果您删除或禁用cookies，我们的网站或应用程序的某些功能可能无法正常实现。我们的一些服务合作伙伴（下文讨论）可能会因为他们代表我们履行的服务而使用他们自己的cookies、匿名标识符或其他跟踪技术。</p>
                <p>&nbsp;</p>
                <p>在涉及我们使用您在访问我们的应用程序时被发送的cookies和显示的其他设备标识符时，我们会告知您。您确认并同意，在不管理您的cookie和设备选择和偏好的情况下继续使用我们的网站或应用程序，即表示您同意我们的cookie和设备标识符设置，并承认您理解我们的cookies政策。</p>
                <p>&nbsp;</p>
                <p><strong>与客户服务团队的沟通</strong></p>
                <p>如果您通过我们的网站或应用程序中的实时聊天工具、电子邮件或电话联系我们的客户服务团队，我们的客户服务顾问可收集您的个人信息，并使用这些信息识别您的客户身份，帮助您查询信息，处理您的订单，交付产品和服务，处理付款，更新我们的记录以及综合管理您根据与我们之间的条款向我们开设的账户。</p>
                <p>&nbsp;</p>
                <p><strong>儿童的隐私</strong></p>
                <p>您必须年满16周岁才能使用网站和应用程序。如果您未满16周岁或者在您的国家或居住地属于未成年人，您的父母或法定监护人必须提供其信息并代表您接受本隐私政策，且您只有在获得您父母或法定监护人许可的情况下才能访问和使用汉莎航空的网站和应用程序。我们不会故意收集16周岁以下或相关司法辖区规定的相应最低年龄以下儿童的个人信息。如果我们意识到我们已经无意中收到了16岁以下或相关司法辖区规定的相应最低年龄以下儿童的个人信息，我们将从我们的记录中删除这些信息。</p>
                <p>&nbsp;</p>
                <p><strong>广告和在线跟踪</strong></p>
                <p>&nbsp;</p>
                <p>当您访问我们的网站时，我们可能会允许第三方公司投放广告并收集某些匿名信息。这些公司可使用您在访问我们的网站和其他网站期间留下的无法识别个人身份的信息（例如，点击流信息、网络浏览器类型、时间和日期、点击广告或滚动广告的对象），以便提供您可能感兴趣的商品和服务方面的广告。这些公司通常使用cookie收集这些信息。我们的系统无法识别浏览器的&ldquo;不跟踪&rdquo;信号，但使用我们网站上的这些cookies的几个服务合作伙伴可供您选择不接收有针对性的广告。您若选择不接收此类广告，您可通过这些工具选择不接收我们的市场营销信息（请参阅下面的&ldquo;退订汉莎航空推送信息&rdquo;）。</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p><strong>汉莎航空推送信息</strong></p>
                <p>&nbsp;</p>
                <p>我们只能在您同意的情况下向您发送有关我们产品、服务和活动的汉莎航空推送信息。即使您已经同意接收这类推送信息，您也可以随时取消（请参阅下面的&ldquo;退订汉莎航空推送信息&rdquo;）。如果您已同意接收推送信息，我们将通过您首选的通讯方式（例如通过电子邮件、邮寄、电话或短信服务）向您发送汉莎航空推送信息。如果您在应用程序或移动设备上启用了推送通知，我们会向您的手机发送汉莎航空推送信息。</p>
                <p>&nbsp;</p>
                <p>我们也可在您参与我们的调查或活动、您在社交平台上发布帖子或者以其他方式与我们的网页互动的地方获得您的个人信息，且作为回应，我们可利用这些信息与您进行交流或通过社交媒体平台向您提供有关我们产品、活动和推广活动的相关内容，只要适用法律允许。请参阅相关社交媒体平台的隐私政策，了解他们如何收集和使用您的个人信息。</p>
                <p>&nbsp;</p>
                <p><strong>退订汉莎航空推送信息</strong></p>
                <p>&nbsp;</p>
                <p>如果您已注册接收有关我们产品、服务和活动的汉莎航空推送信息，但您希望不再收到这些推送信息，您可以请我们修改您的选择。您可按推送信息的具体发送方式中说明的方式提出请求，例如，通过使用所有电子邮件推送信息中均包含的退订链接提出请求，或者通过在线修改您的选择提出请求（请参阅下文&ldquo;您对个人信息享有的权利&rdquo;）。由于进行变更处理需要时间，汉莎航空将在合理时间内且在任何情况下在收到您的请求后30日内停止向您发送您不再同意接收的推送信息。您可通过移动设备上的应用程序更改相关程序选择不接收推送通知。</p>
                <p>&nbsp;</p>
                <p><strong>我们与谁共享您的个人信息</strong></p>
                <p>在不违反适用法律或任何适用之司法管辖区法律法规的前提下，我们可与我们所在的集团在全球的其他公司（包括合资企业，合称为&ldquo;<strong>关联公司</strong>&rdquo;）以及选定的服务提供商（包括特许经营商和被许可方）共享您的个人信息。在适用法律许可的情况下或（如果适用法律要求取得您的同意，则在）获得您同意后，这些服务提供商（&ldquo;<strong>服务提供商</strong>&rdquo;）按下文所述销售或推广汉莎航空产品和服务或代表我们履行其他职能（如市场推广和数据分析）。此外，我们还可共享无法通过其直接识别您身份的汇总信息或匿名信息。请参阅下面的隐私政策的各项条款，以便了解更多与我们可在何时与谁共享您的信息相关的信息。</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                <p><strong>第三方供应商</strong></p>
                <p>当您访问网站或应用程序时，我们可能会允许第三方供应商收集和处理某些匿名化和非个人化信息，包括但不限于设备信息和小文件信息。这些公司可使用您在访问网站和应用程序、提交页面评级、和访问其他网站期间留下的无法识别个人身份的信息（例如，点击流信息、网络浏览器类型、时间和日期、点击广告或滚动广告的对象），以便提供您可能感兴趣的商品和服务方面的广告。我们确保该提交给第三方供应商的信息并不与具体个人相关，其在进行分析时，为统计目的，仅对匿名化、去标识化和综合数据加以评估。</p>
                <p>&nbsp;</p>
                <p><strong>服务合作伙伴</strong></p>
                <p>&nbsp;</p>
                <p><strong>金融机构和付款处理合作伙伴</strong></p>
                <p>&nbsp;</p>
                <p>使用我们的网站或应用程序进行的付款，是通过我们的付款解决方案提供商实现的。届时您将直接向我们的供应商提供信用卡或借记卡信息，供应商将进一步处理付款细节。</p>
                <p>&nbsp;</p>
                <p><strong>依法披露</strong></p>
                <p>我们承诺我们的雇员、供应商、服务合作伙伴都将根据适用法律的规定履行保密义务。在某些情况下（如适用法律要求或允许，或者（在适用法律要求的情况下）获得您的同意后），我们可出于以下目的将与您有关的个人信息披露给政府管理机构及办公室或其他司法管辖区的第三方：遵守任何司法管辖区域内的法律要求，遵循法定流程或监管流程，获得法律意见，减少信用风险，防止和发现欺诈和/或保护和维护汉莎航空和汉莎航空集团其他成员的权利和财产。为了降低信用风险和防止欺诈，我们每次披露您的信息时，都会采取一切合理必要的措施以帮助确保信息安全。</p>
                <p>&nbsp;</p>
                <p><strong>因业务或流程而变更合作伙伴</strong></p>
                <p>如果我们或我们的部分业务发生重组或者被出售或许可给第三方，我们所持有的关于您的任何个人信息可能会转移给该重组后存续的实体、被许可方或第三方。</p>
                <p>&nbsp;</p>
                <p><strong>向国外转移数据</strong></p>
                <p>对于您通过我们的网站和/或应用程序提供的个人信息，在位于中国的服务器上进行处理并存储。</p>
                <p>&nbsp;</p>
                <p>在遵守适用法律或任何适用之司法管辖区法律法规的前提下，我们可将向您收集的信息转移给我们集团内其他公司、合资企业、特许经营商、被许可方、以及选定的代表我们履行（上述每一项）职能的服务提供商和合作伙伴，他们位于除中国、德国、欧洲经济区（&ldquo;EEA&rdquo;）或您居住国以外的国家，并且所转移的信息可在这些国家存储和处理。</p>
                <p>&nbsp;</p>
                <p>对于存储在其他司法管辖区的个人信息，可能需遵循自该司法辖区的政府、法院或执法机构根据其法律提出的访问请求。无论您向我们提供的个人信息是由我们或我们的关联公司还是服务提供商在中国或德国、欧洲经济区之内还是之外进行处理，我们都将采取措施确保您的个人信息所获得的保护达到本隐私政策、相关司法辖区的数据保护法要求我们提供的保护级别，并符合适用法律和相关相关司法辖区法律认可的现行数据转移机制。</p>
                <p><strong>&nbsp;</strong></p>
                <p><strong>您对您个人信息享有的权利</strong></p>
                <p>您可依照适用法律享有以下权利：要求提供我们持有的关于您的个人信息的细节，修改、限定或删除您的个人信息，或要求获得您给我们的个人信息的副本。您也可以按照本隐私政策撤回您给予我们的任何同意和反对我们使用您的个人信息。</p>
                <p>&nbsp;</p>
                <p>如果您想对我们持有的关于您的个人信息行使上述权利，或者希望随时更改您的选择，请随时与我们联系（请参阅下文&ldquo;联系汉莎航空&rdquo;）。如果您对我们作出的回复不满意，您还可将相关问题提交给相关数据保护监督机构以寻求帮助。</p>
                <p><strong>&nbsp;</strong></p>
                <p><strong>个人信息的保留和删除</strong></p>
                <p>只有在适用法律要求的情形下，或者在我们的业务经营需要或适用法律要求的情形下，我们才会保留您的信息。但为了执行我们的条款、防止欺诈以及确认、提出或者解决合法的权利主张和/或为了妥善保管记录之目的，即使客户账户已被关闭或删除，我们仍可能需要保留某些个人信息。删除或销毁您的个人信息时将采用无法恢复或复制的方式。</p>
                <p><strong>&nbsp;</strong></p>
                <p><strong>安全和第三方链接</strong></p>
                <p>我们采取合理措施保护您的个人信息免遭未经授权或意外的访问、处理、删除、丢失或使用。然而，通过互联网传输信息并不完全安全。尽管我们尽力保护您的个人信息，但我们无法保证您提交给我们的个人信息的安全性，信息传输方面的任何风险均将由您自己承担。</p>
                <p>但是，我们使用严格的流程和安全功能以尽可能防止未经授权的访问。我们组织内的人员或我们的第三方服务合作伙伴可能会访问个人信息，以便实现上述目的或适用法律允许或要求的其他目的。</p>
                <p>&nbsp;</p>
                <p>我们的网站和/或应用程序可能会不时包含连接到不受我们控制的其他网站的链接。对于该等其他网站的隐私惯常处理方式、收集的数据或内容，我们不承担任何责任或义务。这些链接指向的网站的运营商没有义务遵守本隐私政策。如果那些网站上出现条款和条件、其他隐私声明或政策，您还应该仔细阅读，因为您对这些网站的使用可能需要遵守该等条款和条件、其他隐私声明或政策。</p>
                <p>&nbsp;</p>
                <p>您确认，我们不对不可抗力及基于网络特殊属性发生的黑客攻击、计算机病毒侵入、电信部门技术调整、腾信微信平台或相关系统的稳定性、您将个人账号信息告知他人、或与任何第三人间的违约或侵权行为等我们无法控制的情况承担责任。</p>
                <p><strong>&nbsp;</strong></p>
                <p><strong>本隐私政策的变更</strong></p>
                <p>我们可能会不时更新本隐私政策。经修订的隐私政策将发布在我们的网站或应用程序上。请定期查看本页面以了解本隐私政策方面的变更。如果我们对本隐私政策作出任何重大变更，我们将在通过我们的网站或应用程序或者通过向您发送通知的方式，在其生效前通知您。任何此类重大变更仅适用于经修订的隐私政策生效后所收集的个人信息。</p>
                <p><strong>&nbsp;</strong></p>
                <p><strong>联系汉莎航空</strong></p>
                <p>如果您对收集、储存或使用您的个人信息的方式有任何疑问，而这些疑问在本隐私政策未得到解答，您可以联系我们。请通过拨打4008-868-868与我们联系。</p>
                <p>&nbsp;</p>
                <p><strong>适用法律和争议解决</strong></p>
                <p>本隐私政策之执行及争议解决等均适用中华人共和国大陆法律。任何争议无法协商一致的，应提交至我们所在地人民法院以诉讼方式解决。</p>
                <p>&nbsp;</p>
                <p><strong>腾讯的隐私政策</strong></p>
                <p>您应同时遵守腾讯的隐私政策，但若本隐私政策与腾讯的隐私政策相冲突的，我们和用户之间的关系应受本隐私政策调整。</p>
                <p>&nbsp;</p>
                <p>我们保留对本隐私政策的最终解释权。</p>




            </div>
        </div>


        <div class="content rule2">
            <div class="header">
                活动规则
                <img class="closebtn" src="{{asset('public/web/images/closebtn.png')}}" />
            </div>
            <div class="body">
                <div>一、活动时间：</div>
                <div style="margin-bottom: 10px;">北京时间2018年12月20日至北京时间2019年1月30日</div>

                <div>二、参与方式：</div>
                <div style="margin-bottom: 10px;">进入H5参与活动，选择2018去过的旅行地，生成自己的年度轨迹，即有机会赢取活动奖品。</div>

                <div>三、奖品设置：</div>
                <div>一等奖：Rimowa旅行箱 1个</div>
                <div>二等奖：汉莎航空定制健身包 20 个</div>
                <div style="margin-bottom: 10px;">三等奖：汉莎航空#现在为世界出发 行李袋50个</div>

                <div>四、奖项公布：</div>
                <div style="margin-bottom: 10px;">2019年1月31日在汉莎航空官方微信微博公布中奖名单。</div>

                <div>五、奖品领取事项：</div>
                <div style="margin-bottom: 10px;">中奖名单公布后，若中奖粉丝无法在7个工作日内回复汉莎航空领取奖品，则视为自动放弃。</div>

                <div>六、数据收集：</div>
                <div>汉莎航空将只收集用于举办活动和联系获奖者的数据，相关数据不会用于其他行销活动。</div>

                <div>* 参加汉莎航空“与世界Say Yes：我的2018旅行轨迹 ”活动即代表您已阅读并认同活动相关规则和法律条款。</div>

            </div>
        </div>
    </div>
</div>
<div class="step-03 container">
    <div class="fullscreen">
        <img src="{{asset('public/web/images/step_03_bg.jpg')}}"/>
    </div>
    <div class="logo"><img src="{{asset('public/web/images/logo.png')}}"/></div>
    <div class="content-box">
        <div class="dist-area">

        </div>
        <div class="city-list"></div>
        <div class="button" onclick="gtag('event', 'Button', { 'event_category': 'Citypage', 'event_label': 'Yes' });">
            <img src="{{asset('public/web/images/step_03_button.png')}}"/>
        </div>
    </div>
</div>
<div class="step-04 container">
    <div class="fullscreen">
        <img class="world" src="{{asset('public/web/images/step_04_bg.jpg')}}"/>
        <img class="china" src="{{asset('public/web/images/step_04_bg_china.jpg')}}"/>
    </div>
    <div class="logo"><img src="{{asset('public/web/images/logo.png')}}"/></div>
    <div class="content-box">
        <div class="frame" style='z-index:110'>
            <img id="frameimagetosave"/>
        </div>
        <div class="frame" id="frame" style='z-index:100'>
            <img id="frameimage" src="{{asset('public/web/images/step_04_frame_china.png')}}"/>
            <canvas id="canvas"></canvas>
        </div>
        <div class="longpressbtn-box" style='z-index:105' onclick="gtag('event', 'Button', { 'event_category': ‘Sharepage', 'event_label': 'Save' });">
            <img class="btn longpressbtn" src="{{asset('public/web/images/step_04_long_press_button.png')}}" />
        </div>
        <div class="show-box" onclick="gtag('event', 'Button', { 'event_category': ‘Sharepage', 'event_label': 'Show' });">
            <img class="btn showsbtn" src="{{asset('public/web/images/step_04_show_button.png')}}" />
        </div>
    </div>
    <div class="fullscreen plain-mask" style="display: none;z-index:130">
        <img src="{{asset('public/web/images/step_04_plain_mask.png')}}" />
    </div>
    <div class="content-box plane-content-box" style='z-index:141;display: none;'>
        <div class="plain-text-box" style="">
            <img src="{{asset('public/web/images/step_04_plain_text.png')}}" />
        </div>
    </div>
</div>

@if(is_wechat())
    <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        var WX_APPID = "{{ config('app.wx_app_id') }}";
        console.log(WX_APPID);
    </script>
    <script src="{{ asset('public/js/wechat.js') }}"></script>
    <script type="text/javascript">
        /* 自主生成wx.config */
        var js_ticket = "{{ $js_ticket }}";
        wxconfigSet(js_ticket);
    </script>
@endif
</body>
<script>
    const ua = navigator.userAgent.toLowerCase();
    if (window.location.href.indexOf('debug') > -1) {
        new VConsole();
    }
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113308626-17"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-113308626-17');
</script>
</html>