<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>欢迎页面-后台管理系统</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="stylesheet" href="{{asset('public/admin/static/css/font.css')}}">
    <link rel="stylesheet" href="{{asset('public/admin/static/css/weadmin.css')}}">
    <script src="{{asset('public/admin/lib/layui/layui.js')}}" charset="utf-8"></script>
    @yield('head')
</head>
<body>
    @yield('nav')
    <div class="weadmin-body">
        @yield('body')
    </div>
</body>
<script type="text/javascript">
    layui.extend({
        admin: "{{asset('/public/admin/static/js/admin')}}",
    });
</script>
</html>