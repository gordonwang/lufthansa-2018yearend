<!-- 左侧菜单开始 -->
<div class="left-nav">
    <div id="side-nav">
        <ul id="nav">
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe6b8;</i>
                    <cite>足迹查看</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="{{route('users_city')}}" name="category">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>足迹</cite>
                        </a>
                    </li>
                    <li>
                        <a _href="{{route('member')}}" name="task">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>用户信息</cite>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- <div class="x-slide_left"></div> -->
<!-- 左侧菜单结束 -->