@extends('admin.common.base')
@section('nav')
<div class="weadmin-nav">
    <span class="layui-breadcrumb">
        <a><cite>首页</cite></a>
        <a><cite>足迹查看</cite></a>
        <a><cite>用户列表</cite></a>
      </span>
    <a class="layui-btn layui-btn-sm" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
        <i class="layui-icon" style="line-height:30px">ဂ</i>
    </a>
</div>
@endsection
@section('body')
    <div class="memberTable">

        <div class="layui-inline">
            <div class="layui-input-inline">
                <input class="layui-input" placeholder="时间筛选" name="start_time" id="start_time">
            </div>

        </div>
        <button class="layui-btn" data-type="reload">搜索</button>

    </div>

    <table class="layui-hide" id="member_table" lay-filter="member_table"></table>

    <script type="text/html" id="buttonTop">
    </script>
    <script type="text/javascript" src="{{asset('public/admin/js/City/memberlist.js')}}"></script>
@endsection