layui.use(['table','element','admin','laydate'], function(){
    var table = layui.table;
    var element = layui.element;
    var  laydate = layui.laydate;
    //方法级渲染
    table.render({
        elem: '#category_table'
        ,url:"users_city_search"
        ,id: 'categoryReload'
        ,page: true
        ,limits:[10,100,500,1000,2000,3000]
        ,toolbar: '#buttonTop'
        ,parseData: function(res){ //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.count, //解析数据长度
                "data": res.data.result //解析数据列表
            };
        }
        ,cols: [[
            {field:'mid', title: '用户id', sort: true}
            ,{field:'openid', title: 'openid'}
            ,{field:'head', title: '头像',templet:function (res) {
                    return '<div><img src="'+res.head+'"></div>'
                }
            }
            ,{field:'nick_name',  title: '昵称'}
            ,{field:'city',  title: '城市'}
            ,{field:'img', title: '地图',templet:function (res) {
                    return '<div><img src="http://'+res.img+'"></div>'
                }
            }
            ,{field:'created_at', title: '创建时间', sort: true}
            ,{field:'updated_at', title: '添加足迹时间', sort: true}
        ]]
    });

    //日期时间范围
    laydate.render({
        elem: '#start_time'
        ,type: 'datetime'
        ,range: '~'
    });

    //加载刷新
    var $ = layui.$, active = {
        reload: function(){
            var start_time = $('#start_time').val();
            //执行重载
            table.reload('categoryReload', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                ,where: {
                    start_time:start_time,
                }
            });
        }
    };
    $('.categoryTable .layui-btn').on('click', function(){
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });
});