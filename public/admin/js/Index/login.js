var PRIVATE_Key = '2018_lufthansa';
layui.extend({
    admin: "/lufthansa/2018yearend/public/admin/static/js/admin"
});

layui.use(['form','admin','jquery'], function(){
    var form = layui.form
        ,admin = layui.admin
        ,$ = layui.jquery;
    //监听提交
    form.on('submit(login)', function(data){
        var myData = data.field;
        var params = {};
        params.username = myData.username;
        params.password = hex_md5(myData.password + PRIVATE_Key);
        $.post("checkLogin",params,function (res) {
            if(res.code == 200){
                location.href='admin_index';
            }else{
                layer.msg(res.msg);
            }
        },'json');
    });
});