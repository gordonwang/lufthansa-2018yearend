var ts = Math.round(+new Date() / 1000);
var signature = sha1();
var urls = location.href.split('#')[0];
var n = 0;

var share_title  = 'Say yes to the world，向朋友晒出你的2018旅行轨迹！';
var share_link   = "http://www.mlpsh.cn/lufthansa/2018yearend";
var share_desc   = '标记旅行轨迹赢Rimowa行李箱，汉莎航空伴你回顾全年旅程。';
var share_image  = 'http://www.mlpsh.cn/lufthansa/2018yearend/public/web/images/sharing.jpg';


var wx_appid = WX_APPID || '';

function wxconfigSet(js_ticket) {
  var ts = Math.round(+new Date() / 1000);
  var urls = location.href.split('#')[0];
  var snKey = "jsapi_ticket=" + js_ticket + "&noncestr=Wm3WZYTPz0wzccn&timestamp=" + ts + "&url=" + urls;
  var signature = sha1(snKey);

  wx.config({
    debug: false,
    appId: wx_appid,
    timestamp: ts,
    nonceStr: "Wm3WZYTPz0wzccn",
    signature: signature,

    jsApiList: [
      'checkJsApi',
      'onMenuShareTimeline',
      'onMenuShareAppMessage',
      'scanQRCode'
    ]
  });


  wx.ready(function () {

    wx.checkJsApi({
      jsApiList: [
        'onMenuShareTimeline',
        'onMenuShareAppMessage'
      ]
    });

    wx.showOptionMenu();

    //分享给朋友
    wx.onMenuShareAppMessage({
      title: share_title, // 分享标题
      desc:  share_desc, // 分享描述
      link:  share_link,
      imgUrl: share_image, // 分享图标
      type: '', // 分享类型,music、video或link，不填默认为link
      dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
      trigger: function (res) {
      },
      success: function () {
        // debugger;
        if ($("[data-state='tips']").length > 0 && $("[data-state='tips']").length) {
          $("[data-state='tips']").hide();
          $("[data-state='invitation-img']").css({"right": "100%", "top": "50%"});
          $("[data-state='transmit-img']").css({"right": "100%", "top": "50%"});
        }
        //分享给朋友
          if (typeof(gtag) == 'function') {
              gtag('event', 'Button', { 'event_category': 'Sharepage', 'event_label': 'Friends' });
              if (state_page_url.length && state_page_url.length > 0) {
                  //分享
                  fa('send', 'show-share-friends', '展示-分享好友');
              } else {
                  //通用
                  fa('send', 'com-share-friends', '通用-分享好友');
              }
          }

      },
      cancel: function () {
        // debugger;
        if ($("[data-state='tips']").length > 0 && $("[data-state='tips']").length) {
          $("[data-state='tips']").hide();
          $("[data-state='invitation-img']").css({"right": "100%", "top": "50%"});
          $("[data-state='transmit-img']").css({"right": "100%", "top": "50%"});
        }
        // $('.mask').hide();
        // ga('send', 'event', 'wShare', 'AppMessage', 'cancel');
      },
      fail: function (res) {
        // debugger;
      }
    });
    //分享到朋友圈
    wx.onMenuShareTimeline({
      title: share_title,
      link:  share_link,
      imgUrl: share_image,
      trigger: function (res) {
        // alert('用户点击分享到朋友圈' +JSON.stringify(res));
        // debugger;
      },
      success: function (res) {
        // alert('已分享');
        if ($("[data-state='tips']").length > 0 && $("[data-state='tips']").length) {
          $("[data-state='tips']").hide();
          $("[data-state='invitation-img']").css({"right": "100%", "top": "50%"});
          $("[data-state='transmit-img']").css({"right": "100%", "top": "50%"});
        }
        if (typeof(gtag) == 'function') {
            gtag('event', 'Button', { 'event_category': 'Sharepage', 'event_label': 'Moments' });
            if (state_page_url.length && state_page_url.length > 0) {
                //分享
                fa('send', 'show-share-moments', '展示-分享朋友圈');
            } else {
                //通用
                fa('send', 'com-share-moments', '通用-分享朋友圈');
            }
        }
      },
      cancel: function (res) {
        // alert('已取消');
        if ($("[data-state='tips']").length > 0 && $("[data-state='tips']").length) {
          $("[data-state='tips']").hide();
          $("[data-state='invitation-img']").css({"right": "100%", "top": "50%"});
          $("[data-state='transmit-img']").css({"right": "100%", "top": "50%"});
        }
        // $('.mask').hide();
        // ga('send', 'event', 'wShare', 'Timeline', 'cancel');
      },
      fail: function (res) {
        // alert('wx.onMenuShareTimeline:fail: '+JSON.stringify(res));
        // $('.mask').hide();
      }
    });
  });
  wx.error(function (res) {
    //alert('wx.error: ' + JSON.stringify(res));
  });
}

function sha1(str) {
  var rotate_left = function (n, s) {
    var t4 = (n << s) | (n >>> (32 - s));
    return t4;
  };
  var cvt_hex = function (val) {
    var str = '';
    var i;
    var v;

    for (i = 7; i >= 0; i--) {
      v = (val >>> (i * 4)) & 0x0f;
      str += v.toString(16);
    }
    return str;
  };

  var blockstart;
  var i, j;
  var W = new Array(80);
  var H0 = 0x67452301;
  var H1 = 0xEFCDAB89;
  var H2 = 0x98BADCFE;
  var H3 = 0x10325476;
  var H4 = 0xC3D2E1F0;
  var A, B, C, D, E;
  var temp;

  str = this.utf8_encode(str);
  var str_len = str.length;

  var word_array = [];
  for (i = 0; i < str_len - 3; i += 4) {
    j = str.charCodeAt(i) << 24 | str.charCodeAt(i + 1) << 16 | str.charCodeAt(i + 2) << 8 | str.charCodeAt(i + 3);
    word_array.push(j);
  }

  switch (str_len % 4) {
    case 0:
      i = 0x080000000;
      break;
    case 1:
      i = str.charCodeAt(str_len - 1) << 24 | 0x0800000;
      break;
    case 2:
      i = str.charCodeAt(str_len - 2) << 24 | str.charCodeAt(str_len - 1) << 16 | 0x08000;
      break;
    case 3:
      i = str.charCodeAt(str_len - 3) << 24 | str.charCodeAt(str_len - 2) << 16 | str.charCodeAt(str_len - 1) <<
        8 | 0x80;
      break;
  }

  word_array.push(i);

  while ((word_array.length % 16) != 14) {
    word_array.push(0);
  }

  word_array.push(str_len >>> 29);
  word_array.push((str_len << 3) & 0x0ffffffff);

  for (blockstart = 0; blockstart < word_array.length; blockstart += 16) {
    for (i = 0; i < 16; i++) {
      W[i] = word_array[blockstart + i];
    }
    for (i = 16; i <= 79; i++) {
      W[i] = rotate_left(W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16], 1);
    }

    A = H0;
    B = H1;
    C = H2;
    D = H3;
    E = H4;

    for (i = 0; i <= 19; i++) {
      temp = (rotate_left(A, 5) + ((B & C) | (~B & D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B, 30);
      B = A;
      A = temp;
    }

    for (i = 20; i <= 39; i++) {
      temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B, 30);
      B = A;
      A = temp;
    }

    for (i = 40; i <= 59; i++) {
      temp = (rotate_left(A, 5) + ((B & C) | (B & D) | (C & D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B, 30);
      B = A;
      A = temp;
    }

    for (i = 60; i <= 79; i++) {
      temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B, 30);
      B = A;
      A = temp;
    }

    H0 = (H0 + A) & 0x0ffffffff;
    H1 = (H1 + B) & 0x0ffffffff;
    H2 = (H2 + C) & 0x0ffffffff;
    H3 = (H3 + D) & 0x0ffffffff;
    H4 = (H4 + E) & 0x0ffffffff;
  }

  temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);
  return temp.toLowerCase();
}

function utf8_encode(string) {
  return unescape(encodeURIComponent(string));
}

function utf8_decode(string) {
  return decodeURIComponent(escape(string));
}