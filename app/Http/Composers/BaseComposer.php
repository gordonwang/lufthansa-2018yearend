<?php
namespace App\Http\Composers;
use Illuminate\Contracts\View\View;
use App\WeChat\Wechat;
class BaseComposer {

    protected $wechat;
    public function __construct(Wechat $wechat)
    {
        $this->wechat = $wechat;
        $this->wechat->setRedirectUrl(route('authorize.back'));
        $this->wechat->setScope('snsapi_userinfo');
    }

    public function compose(View $view)
    {
        $js_ticket = $this->wechat->getWXJSTicket();
        $view->with('js_ticket', $js_ticket);
        if(is_wechat()){
            $user_open_id = $this->wechat->getOpenID();
        }else{
            $user_open_id = '';
        }
        $view->with('user_open_id', $user_open_id);
    }

}