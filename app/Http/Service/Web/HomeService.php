<?php
namespace App\Http\Service\Web;
use App\Http\Controllers\Controller;

class HomeService extends Controller{

    public function dealAddCity($data){
        $_data = array();
        $num = 0;
        foreach ($data as $data_v){
            if(isset($data_v['id'])){
                $_data[] = $data_v;
                $num++;
            }
        }
        if(!$_data){
            return $this->returnMessage('请选择您去过的城市');
        }
        $map['num'] = $num;
        $map['city'] = serialize($_data);
        return $map;
    }
}