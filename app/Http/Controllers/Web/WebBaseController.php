<?php
/**
 * description:web controller base controller
 * author:lzy
 * date:2018/11/14
 */

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Models\Member;
use App\WeChat\Wechat;
use Illuminate\Support\Facades\Cache;

class WebBaseController extends Controller {
    const PRIVATE_Key = '2018_Lufthansa';
    public $userInfo;
    public $loadWeChat;
    /**
     * describe: is it MicroMessenger
     * author: lzy
     * date: 10/16/2018
     * @param Wechat $wechat
     */
    public function __construct(Wechat $wechat)
    {
        $this->post_method();
        $this->loadWeChat = $wechat;
        $this->loadWeChat->setRedirectUrl(route('authorize.back'));
        $this->loadWeChat->setScope('snsapi_userinfo');

        //获取用户信息
        $this->middleware(function ($request, $next) {
            if(!$this->userInfo){
                $this->userInfo = $this->is_login();
            }
            return $next($request);
        });
    }

    /**
     * describe: add user session
     * author: lzy
     * date: 10/16/2018
     * @return mixed
     */
    public function is_login(){
        $userInfo = session(self::PRIVATE_Key);
        if(is_wechat()){

            if(!isset($userInfo['openid']) || empty($userInfo['openid'])){
                $userInfo['openid'] = $this->loadWeChat->getOpenId();

                if(!$userInfo['openid'] || $userInfo['openid'] == null) return false;
            }
        }else{
            if(!isset($userInfo['id']) || empty($userInfo['id'])){
                $nowCache = Cache::get(self::PRIVATE_Key.get_client_ip_from_ns());
                if($nowCache){
                    $userInfo = $nowCache;
                    error_log(print_r(get_client_ip_from_ns().'拦截ip
                    ',3),3,'die_ip.log');
                    error_log(print_r($userInfo,3),3,'user.log');
                }else{
                    //添加用户信息
                    $memberModel = new Member();
                    $data['is_wechat'] = 1;
//                $data['nick_name'] = '将心比心';
//                $data['head'] = 'http://www.mlpsh.cn/lufthansa/2018yearend/public/web/images/avatar.jpg';
                    $userInfo['id'] = $memberModel->insertMessage($data);
                    Cache::put(self::PRIVATE_Key.get_client_ip_from_ns(),$userInfo,180);
                }
            }
        }
        session([self::PRIVATE_Key=>$userInfo]);
        return $userInfo;
    }
    /**
     * describe:encrypt link
     * author: lzy
     * date: 10/18/2018
     * @param array $data
     *  @return string
     */
    public function encryptLink($data){
        $url = '';
        //splicing arrays into string
        foreach ($data as $key=>$val){
            $url .= $key . '=' . $val . '&';
        }
        //Encrypted links
        $newUrl = substr($url,0,-1);
        $lastUrl = base64_encode(urlencode($newUrl));
        return $lastUrl;
    }

    /**
     * describe: decrypt link
     * author: lzy
     * date: 10/18/2018
     * @param string $url
     *  @return array
     */
    public function decryptLink($url){
        //split a string into an array
        $newUrl = urldecode(base64_decode($url));
        $data = explode('&',$newUrl);

        $newData = array();
        //reorganized array
        foreach ($data as $val){
            $minData = explode('=',$val);
            $newData[$minData[0]] = $minData[1];
        }
        return $newData;
    }

    /**
     * describe:encrypt data
     * author: lzy
     * date: 10/19/2018
     * @param array $data
     *  @return string
     */
    public function encryptData($data){
        $encrypted = openssl_encrypt($data,'des-ecb',self::PRIVATE_Key);
        return base64_encode($encrypted);
    }

    /**
     * describe: decrypt data
     * author: lzy
     * date: 10/19/2018
     * @param string $str
     * @return array
     */
    public function decryptData($str){
        $encryptedData = base64_decode($str);
        $decrypted = openssl_decrypt($encryptedData,'des-ecb',self::PRIVATE_Key);
        return $decrypted;
    }

    /**
     * describe: decrypt data
     * author: lzy
     * date: 10/19/2018
     * @param $msg
     * @param array $data
     * @param int $code
     * @return json
     */
    public function returnMessage($msg='error',$code=204,$data=array()){
        $result = array(
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        );
        return json_encode($result);
    }

    /**
     * description:接口处理
     * author:lzy
     * date:2017-14-20
     * return:
     */
    public function post_method(){
        header("Content-type: text/html; charset=utf-8");
        header('content-type:application:json;charset=utf8');
        header('Access-Control-Allow-Origin:*');
        header('Access-Control-Allow-Methods:POST');
        header('Access-Control-Allow-Headers:x-requested-with,content-type');
        header('content-type:application:json;charset=utf8');
        header('Access-Control-Allow-Origin:*');
    }
}