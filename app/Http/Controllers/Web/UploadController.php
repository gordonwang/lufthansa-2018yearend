<?php
namespace App\Http\Controllers\Web;
use App\Http\Models\Member;
use App\Http\Models\UsersCity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends WebBaseController {

    /**
     * describe: add img
     * author: lzy
     * date: 10/16/2018
     * @param Request $request
     * @return string
     */
    public function uploadImg(Request $request){
        $file = $request->get('file');
        $cityId = $request->get('id',1);
        if(!$cityId) return $this->returnMessage('参数错误');

        //add image
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $file, $result)){
            $type = $result[2];
            // 拼接文件名称
            $filename = date('YmdHis') . uniqid() . '.' . $type;

            $bool = Storage::disk('upload_company_img')->put($filename, base64_decode(str_replace($result[1], '', $file)));

            if ($bool){
                //update db
                $url=env('APP_URL').'/public/image/'.date('Ym',time()).'/'.$filename;
                $this->updateImg($url,$cityId);

                return $this->returnMessage('上传成功',200,$url);
            }else{
                return false;
            }
        }
    }
    /*public function uploadImg(Request $request)
    {
        error_log(print_r($request->all(),3),3,'2.log');
        $file = $request->file('file');
        $cityId = $request->get('id',1);
        if(!$cityId) return $this->returnMessage('参数错误');
        // 文件是否上传成功
        if ($file->isValid()) {
            // 获取文件相关信息
//            $originalName = $file->getClientOriginalName(); //文件原名
            $ext = $file->getClientOriginalExtension();     // 扩展名

            $realPath = $file->getRealPath();   //临时文件的绝对路径

//            $type = $file->getClientMimeType();     // image/jpeg
            $size =$file->getSize();
            if($size > 2*1024*1024){
                return $this->returnMessage('文件大小超过2M');
            }
            $extArr = array('jpg','jpeg','png','gif');
            if(!in_array($ext,$extArr)){
                return $this->returnMessage('文件格式不正确');
            }

            // 拼接文件名称
            $filename = date('YmdHis') . uniqid() . '.' . $ext;
            // 使用我们新建的upload_company_img本地存储空间（目录）
            //这里的upload_company_img是配置文件的名称
            $bool = Storage::disk('upload_company_img')->put($filename, file_get_contents($realPath));

            if($bool){
                $url=env('APP_URL').'/public/image/'.date('Ym',time()).'/'.$filename;
                $this->updateImg($url,$cityId);
//                $path='/public/image/'.date('Ym',time()).'/'.$filename;
                return $this->returnMessage('上传成功',200,$url);
            }else{
                return $this->returnMessage('上传失败');
            }

        }else{
            return $this->returnMessage('上传失败');
        }
    }*/

    /**
     * describe: add img
     * author: lzy
     * date: 10/16/2018
     * @param $img
     * @param $cityId
     */
    public function updateImg($img,$cityId){
        $openid = $this->loadWeChat->getOpenId();
        if(!$openid){
            $id = $this->userInfo['id'];
            $where['where'] = array('id'=>$id);
        }else{
            $where['where'] = array('openid'=>$openid);
        }
        $_where['where'] = array('id'=>$cityId);
        //new class
        $memberModel = new Member();
        $userCity = new UsersCity();

        //update message
        $memberModel->updateMessage($where,['img'=>$img]);
        $userCity->updateMessage($_where,['img'=>$img]);
    }

}