<?php

namespace App\Http\Controllers\Web;

use App\Http\Models\CommonArray;
use App\Http\Models\Member;
use App\Http\Models\UsersCity;
use App\Http\Service\Web\HomeService;
use Illuminate\Http\Request;

class HomeController extends WebBaseController
{
    private $service;
    private $memberModel;
    private $userCity;
    /**
     * Show the application dashboard.
     * @return view
     */
    public function index()
    {
        return view('web.welcome');
    }

    /**
     * Show the application dashboard.
     * @return view
     */
    public function homePage()
    {
        return view('web.welcome');
    }


    /**
     * describe:encrypt link
     * author: lzy
     * date: 10/18/2018
     * @param Request $request
     * @return json
     */
    public function addCity(Request $request){
        //new class
        $this->service = new HomeService();
        $this->memberModel = new Member();

        //deal message
        $data = $this->service->dealAddCity($request->all());
error_log(print_r($data,3),3,'1.log');
        //get where message
        $openid = $this->loadWeChat->getOpenId();
        if(!$openid){
            $id = $this->userInfo['id'];
            $where['where'] = array('id'=>$id);
        }else{
            $where['where'] = array('openid'=>$openid);
        }

        //update message
        $this->memberModel->updateMessage($where,$data);

        //add user city history
        $cityArr = $this->addUserCity($where,$data);
        return $this->returnMessage('保存成功',200,$cityArr);

    }

    /**
     * describe:encrypt link
     * author: lzy
     * date: 10/18/2018
     * @param $where
     * @param $data
     * @return array
     */
    public function addUserCity($where,$data=array()){
        //new class
        $this->memberModel = new Member();
        $this->userCity = new UsersCity();

        $memberInfo = $this->memberModel->getWhereOne($where);
        if($memberInfo){
            $arr = array(
                'mid' => $memberInfo->id,
                'openid' => $memberInfo->openid,
                'nick_name' => $memberInfo->nick_name,
                'head' => $memberInfo->head,
                'city' => $memberInfo->city,
            );
            $arr['city_id'] = $this->userCity->insertMessage($arr);
            $arr['num'] = $memberInfo->num;
        }else{
            $arr = array(
                'nick_name' => $this->loadWeChat->getNick(),
                'head' => $this->loadWeChat->getHeadImage(),
                'openid' => $this->loadWeChat->getOpenId(),
                'num' => $data['num'],
                'city'=>$data['city'],
                'is_wechat'=>2,
            );
            $this->memberModel->insertMessage($arr);
        }
        $cityArr = unserialize($arr['city']);
        //添加Tag
        $this->addTag($cityArr);

        unset($arr['city']);
        return $arr;
    }

    /**
     * describe:add tag
     * author: lzy
     * date: 10/18/2018
     * @param $cityArr
     */
    public function addTag($cityArr){
        $commonArray = new CommonArray();

        $openid = $this->loadWeChat->getOpenId();
        if(!$openid) $openid = '';
        foreach ($cityArr as $city_v){

            $this->loadWeChat->getTagFollower($commonArray->city[$city_v['name']][0],$commonArray->city[$city_v['name']][1],$openid);
        }
    }

    /**
     * describe:my city
     * author: lzy
     * date: 10/18/2018
     */
    public function myCity(){
        $this->memberModel = new Member();

        //get where message
        $openid = $this->loadWeChat->getOpenId();
        if(!$openid){
            $id = $this->userInfo['id'];
            $where['where'] = array('id'=>$id);
        }else{
            $where['where'] = array('openid'=>$openid);
        }

        $memberInfo = $this->memberModel->getWhereOne($where);
        if($memberInfo){
            $memberInfo->city = unserialize($memberInfo->city);
        }
        return $this->returnMessage('请求成功',200,$memberInfo);
    }
}
