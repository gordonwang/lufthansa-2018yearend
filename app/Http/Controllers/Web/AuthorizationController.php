<?php
namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use App\Http\Models\CommonArray;
use Illuminate\Http\Request;
use App\WeChat\Wechat;

class AuthorizationController extends Controller {
    private $CommonArray;
    private $loadWeChat;
    public function __construct(WeChat $weChat)
    {
        $this->loadWeChat = $weChat;
        $this->loadWeChat->setRedirectUrl(route('authorize.back'));
        $this->loadWeChat->setScope('snsapi_userinfo');
        $this->CommonArray = new CommonArray();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function authorizeBack(Request $request){
        $previous_route = $this->loadWeChat->handleCallback($request);
        return redirect($previous_route);
    }
}