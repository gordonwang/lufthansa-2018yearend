<?php
namespace App\Http\Controllers\Admin\Common;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use OSS\OssClient;

class UploadController extends Controller{
    private $accessKeyId;
    private $accessKeySecret;
    private $endpoint;
    private $bucket;

    public function __construct()
    {
        $this->accessKeyId = config('check.AccessKeyID');
        $this->accessKeySecret = config('check.AccessKeySecret');
        $this->endpoint = config('check.endpoint');
        $this->bucket = config('check.bucket');
    }

    /**
     * description:upload class img
     * author:lzy
     * date:2018/11/22
     * @param Request $request
     * @return \App\Http\Controllers\json
     * @throws \OSS\Core\OssException
     */
    public function uploadImg(Request $request){
        $type = $request->get('type');
        $file = $_FILES['file'];

        //if have img upload,deal img message
        $result = $this->uploadOss($file,$type);
        //return img url
        if($result){
            return $this->returnMessage('上传成功',0,$result['info']['url']);
        }
    }

    /**
     * description:upload editor img
     * author:lzy
     * date:2018/11/22
     * @param $type
     * @return \App\Http\Controllers\json
     * @throws \OSS\Core\OssException
     */
    public function uploadEditor($type){
        $file = $_FILES['file'];

        //if have img upload,deal img message
        $result = $this->uploadOss($file,$type);

        //return img url
        if($result){
            $map['src'] = $result['info']['url'];
            return $this->returnMessage('上传成功',0,$map);
        }
    }

    /**
     * description:upload to oss
     * author:lzy
     * date:2018/11/22
     * @param $file
     * @param $type
     * @return null|string
     * @throws \OSS\Core\OssException
     */
    public function uploadOss($file,$type){
        if($file){
            $filePath = $file['tmp_name'];
            $last = substr($file['name'],strrpos($file['name'],"."));
            $object = $type.'/'.date('Y-m-d',time()).'/img_'.date('Y-m-d',time()).'_'.rand(1000,9999).$last;

            try{
                $ossClient = new OssClient($this->accessKeyId, $this->accessKeySecret, $this->endpoint);
                $result = $ossClient->uploadFile($this->bucket, $object, $filePath);

                return $result;
            } catch(OssException $e) {
                return $e->getMessage();
            }
        }
    }

    /**
     * description:delete file in oss
     * author:lzy
     * date:2018/11/22
     * @param $request
     * @return string
     * @throws \OSS\Core\OssException
     */
    public function deleteFile(Request $request){
        $url = $request->get('fileUrl');

        //delete file
        $this->deleteBiographyFile($url);
    }

    /**
     * description:delete biography file in oss
     * author:lzy
     * date:2018/11/22
     * @param $url
     * @return string
     * @throws \OSS\Core\OssException
     */
    public function deleteBiographyFile($url){
        //replace url
        $url = str_replace(config('check.img_url'),'',$url);
        try{

            $ossClient = new OssClient($this->accessKeyId, $this->accessKeySecret, $this->endpoint);
            $ossClient->deleteObject($this->bucket, $url);
        } catch(OssException $e) {

            return $e->getMessage();
        }
    }
}