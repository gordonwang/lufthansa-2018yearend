<?php
/**
 * description:admin controller base controller
 * author:lzy
 * date:2018/11/14
 */

namespace App\Http\Controllers\Admin\Common;

use App\Http\Controllers\Controller;
use App\Http\Models\UserLog;

class AdminBaseController extends Controller {
    public $userInfo;

    /**
     * description:check login session
     * author:lzy
     * date:2018/12/06
     * AdminBaseController constructor.
     */
    public function __construct() {

        //获取用户信息
        $this->middleware(function ($request, $next) {
            $this->userInfo = $this->is_login();
            return $next($request);
        });

    }
    /**
     * description:check login session
     * author:lzy
     * date:2018/12/06
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function is_login(){
        $userInfo =  json_decode(session(self::SESSION_KEY),true);
        if(!$userInfo){
//            redirect('/admin')->send();
        }
        return $userInfo;
    }

    /**
     * description:log add
     * author:lzy
     * date:2018/12/06
     * @param $data
     * @param string $type
     * @return int
     */
    public function addLog($data, $type = '0')
    {
        //判断是否传入的是数组
        if(!is_array($data)){
            $_arr['handle'] = $data;
        }else{
            $_arr['handle'] = $data['handle'];
        }
        //获取用户ip
        $_arr['ip'] = $this->get_client_ip();
        //获取账号
        if ($type == 0) {
            $_arr['name'] = $this->userInfo['name'];
            $_arr['uid'] = $this->userInfo['id'] * 1;
        } else {
            //当为前台用户时，读取传入的参数
            $_arr['name'] = $data['name'];
            $_arr['uid'] = $data['id'] * 1;
        }
        $_arr['type'] = $type;

        //add user log
        $userLog = new  UserLog();
        $result = $userLog->insertMessage($_arr);
        return $result;
    }

}