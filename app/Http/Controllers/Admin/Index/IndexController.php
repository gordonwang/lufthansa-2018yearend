<?php
namespace App\Http\Controllers\Admin\Index;
use App\Http\Controllers\Admin\Common\AdminBaseController;
use App\Http\Models\Member;
use App\Http\Models\UsersCity;
use Illuminate\Http\Request;

class IndexController extends AdminBaseController{

    /**
     * description:index page
     * author:lzy
     * date:2018/11/14
     */
    public function index(){
        return view('admin.index.admin_index');
    }

    /**
     * description:welcome page
     * author:lzy
     * date:2018/11/14
     */
    public function welcome(){
        return view('admin.index.welcome');
    }

    /**
     * description:usersCity page
     * author:lzy
     * date:2018/11/14
     */
    public function usersCity(){
        return view('admin.city.list');
    }


    public function userCitySearch(Request $request){
        $userCity = new  UsersCity();

        //whewre 条件 --start---
        $where = array();
        $time = $request->get('start_time');
        $start_time = trim(substr($time,0,strrpos($time,'~')));
        $end_time = trim(substr($time,strrpos($time,'~') + 1));
        if($start_time && $end_time) $map['created_at'] = array('>',$start_time.' and `created_at` < '.$end_time);
        if(isset($map)) $where['where'] = $map;
        //where 条件 ----end---

        $cityList['count'] = $userCity->getWhereCount($where);

        $pageSize = $request->get('limit');
        if($pageSize != 1){
            $where['pageSize'] = $request->get('limit');
            $page = $request->get('page');
            $where['offset'] = ($page - 1) * $where['pageSize'];
        }

        $cityList['result'] = $userCity->getWhereMessage($where);
        foreach ($cityList['result'] as &$v){
            $city = unserialize($v['city']);
            $v['city'] = '';
            if($city){
                foreach ($city as $c_v){
                    $v['city'] .= $c_v['name'].'、';
                }
            }
        }

        return $this->returnMessage('请求成功',0,$cityList);
    }

    /**
     * description:member page
     * author:lzy
     * date:2018/11/14
     */
    public function member(){
        return view('admin.city.memberlist');
    }

    public function memberSearch(Request $request){
        $member = new Member();

        //whewre 条件 --start---
        $where = array();
        $time = $request->get('start_time');
        $start_time = trim(substr($time,0,strrpos($time,'~')));
        $end_time = trim(substr($time,strrpos($time,'~') + 1));
        if($start_time && $end_time) $map['created_at'] = array('>',$start_time.' and `created_at` < '.$end_time);
        if(isset($map)) $where['where'] = $map;
        //where 条件 ----end---

        $memberList['count'] = $member->getWhereCount($where);


        $pageSize = $request->get('limit');
        if($pageSize != 1){
            $where['pageSize'] = $request->get('limit');
            $page = $request->get('page');
            $where['offset'] = ($page - 1) * $where['pageSize'];
        }

        $memberList['result'] = $member->getWhereMessage($where);
        foreach ($memberList['result'] as &$v){
            $city = unserialize($v['city']);
            $v['city'] = '';
            if($city){
                foreach ($city as $c_v){
                    $v['city'] .= $c_v['name'].'、';
                }
            }
        }

        return $this->returnMessage('请求成功',0,$memberList);
    }
}