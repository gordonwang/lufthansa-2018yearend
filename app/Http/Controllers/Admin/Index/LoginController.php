<?php
/**
 * description:deal login message
 * author:lzy
 * date:2018/11/14
 */
namespace App\Http\Controllers\Admin\Index;
use App\Http\Controllers\Controller;
use App\Http\Models\Admin;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    protected $adminModel;

    /**
     * description:login page
     * author:lzy
     * date:2018/11/14
     */
    public function login()
    {
        if(session(self::SESSION_KEY)){
            return redirect()->route('admin_index');
        }
        return view('admin.index.login');
    }

    /**
     * description:check login
     * author:lzy
     * date:2018/11/15
     * @param Request $request
     * @return \App\Http\Controllers\json
     */
    public function checkLogin(Request $request){
        $this->adminModel = new Admin();
        $data = $request->all();

        $adminInfo = $this->adminModel->getAdmin('name',$data['username']);

        if(!$adminInfo){
            return $this->returnMessage('用户账号不存在，请核对后再登录');
        }
        if(md5($data['password']) != $adminInfo->password){
            return $this->returnMessage('密码错误，请核对后再输入');
        }

        session([self::SESSION_KEY=>json_encode($adminInfo)]);

        return $this->returnMessage('登录成功',200);
    }

    /**
     * description:login out
     * author:lzy
     * date:2018/12/06
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function loginOut(Request $request){
        $request->session()->forget(self::SESSION_KEY);
        return redirect()->route('login');
    }
}