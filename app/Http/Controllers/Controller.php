<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    const PRIVATE_Key = '2018_four_seasons';
    const SESSION_KEY = 'four_seasons_user_info';

    /**
     * describe: 返回的数据
     * author: lzy
     * date: 10/19/2018
     * @param $msg
     * @param array $data
     * @param int $code
     * @return json
     */
    protected function returnMessage($msg='error',$code=204,$data=array()){
        $result = array(
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        );
        return $this->returnAjax($result);
    }

    /**
     * description:返回AJAX
     * author:lzy
     * parameter:@type 参数类型
     * date:2018/11/14
     * return:
     */
    protected function returnAjax($data) {
        header('Content-Type:application/json; charset=utf-8');
        exit(json_encode($data));
    }

    /**
     * description:跨域处理
     * author:lzy
     * date:2018/11/14
     * return:
     */
    protected function post_method(){
        header("Content-type: text/html; charset=utf-8");
        header('content-type:application:json;charset=utf8');
        header('Access-Control-Allow-Origin:*');
        header('Access-Control-Allow-Methods:POST');
        header('Access-Control-Allow-Headers:x-requested-with,content-type');
        header('content-type:application:json;charset=utf8');
        header('Access-Control-Allow-Origin:*');
    }

    /**
     * describe:encrypt data
     * author: lzy
     * date: 10/19/2018
     * @param array $data
     *  @return string
     */
    protected function encryptData($data){
        $encrypted = openssl_encrypt($data,'des-ecb',self::PRIVATE_Key);
        return base64_encode($encrypted);
    }

    /**
     * describe: decrypt data
     * author: lzy
     * date: 10/19/2018
     * @param string $str
     * @return array
     */
    protected function decryptData($str){
        $encryptedData = base64_decode($str);
        $decrypted = openssl_decrypt($encryptedData,'des-ecb',self::PRIVATE_Key);
        return $decrypted;
    }

    /**
     * description:处理用户密码
     * author:lzy
     * date:2018/11/14
     * @param $password
     * @return string
     */
    protected function treat_user_pwd($password){
        return md5(md5($password.self::PRIVATE_Key));
    }
}
