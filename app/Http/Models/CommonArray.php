<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;

class CommonArray extends Model{

    const TAG_NAME = 'lh_2018yearend_html5_';
    const CHINA_TAG_GROUP = 'Destinations_in_China';
    const ASIA_TAG_GROUP = 'Asia';
    const EUROPE_TAG_GROUP = 'Europe';
    const NORTH_TAG_GROUP = 'North_America';
    const SOUTH_TAG_GROUP = 'South_America';
    const AFRICA_TAG_GROUP = 'Africa';
    const OCEANIA_TAG_GROUP = 'Oceania';

    public $city = array(
        '哈尔滨'=>array('Harbin',self::CHINA_TAG_GROUP),
        '沈阳'=>array('Shenyang',self::CHINA_TAG_GROUP),
        '乌鲁木齐'=>array('Urumchi',self::CHINA_TAG_GROUP),
        '北京'=>array('Beijing',self::CHINA_TAG_GROUP),
        '西安'=>array('Xian',self::CHINA_TAG_GROUP),
        '武汉'=>array('Wuhan',self::CHINA_TAG_GROUP),
        '上海'=>array('Shanghai',self::CHINA_TAG_GROUP),
        '杭州'=>array('Hangzhou',self::CHINA_TAG_GROUP),
        '成都'=>array('Chengdu',self::CHINA_TAG_GROUP),
        '重庆'=>array('Chongqing',self::CHINA_TAG_GROUP),
        '拉萨'=>array('Lhasa',self::CHINA_TAG_GROUP),
        '广州'=>array('Guangzhou',self::CHINA_TAG_GROUP),
        '福州'=>array('Fuzhou',self::CHINA_TAG_GROUP),
        '三亚'=>array('Sanya',self::CHINA_TAG_GROUP),
        '昆明'=>array('Kunming',self::CHINA_TAG_GROUP),
        '长沙'=>array('Changsha',self::CHINA_TAG_GROUP),
        '日本'=>array('Japan',self::ASIA_TAG_GROUP),
        '韩国'=>array('Korea',self::ASIA_TAG_GROUP),
        '土耳其'=>array('Turkey',self::ASIA_TAG_GROUP),
        '尼泊尔'=>array('Nepal',self::ASIA_TAG_GROUP),
        '以色列'=>array('Israel',self::ASIA_TAG_GROUP),
        '阿联酋'=>array('United_Arab_Emirates',self::ASIA_TAG_GROUP),
        '印度'=>array('India',self::ASIA_TAG_GROUP),
        '斯里兰卡'=>array('Sri_Lanka',self::ASIA_TAG_GROUP),
        '菲律宾'=>array('the_Philippines',self::ASIA_TAG_GROUP),
        '越南'=>array('Vietnam',self::ASIA_TAG_GROUP),
        '泰国'=>array('Thailand',self::ASIA_TAG_GROUP),
        '柬埔寨'=>array('Cambodia',self::ASIA_TAG_GROUP),
        '马来西亚'=>array('Malaysia',self::ASIA_TAG_GROUP),
        '马尔代夫'=>array('Maldives',self::ASIA_TAG_GROUP),
        '新加坡'=>array('Singapore',self::ASIA_TAG_GROUP),
        '缅甸'=>array('Myanmar',self::ASIA_TAG_GROUP),
        '挪威'=>array('Norway',self::EUROPE_TAG_GROUP),
        '爱尔兰'=>array('Ireland',self::EUROPE_TAG_GROUP),
        '瑞典'=>array('Sweden',self::EUROPE_TAG_GROUP),
        '芬兰'=>array('Finland',self::EUROPE_TAG_GROUP),
        '英国'=>array('United_Kingdom',self::EUROPE_TAG_GROUP),
        '荷兰'=>array('Netherlands',self::EUROPE_TAG_GROUP),
        '德国'=>array('Germany',self::EUROPE_TAG_GROUP),
        '比利时'=>array('Belgium',self::EUROPE_TAG_GROUP),
        '俄罗斯'=>array('Russia',self::EUROPE_TAG_GROUP),
        '捷克'=>array('Czech_Republic',self::EUROPE_TAG_GROUP),
        '奥地利'=>array('Austria',self::EUROPE_TAG_GROUP),
        '法国'=>array('France',self::EUROPE_TAG_GROUP),
        '瑞士'=>array('Switzerland',self::EUROPE_TAG_GROUP),
        '葡萄牙'=>array('Portugal',self::EUROPE_TAG_GROUP),
        '西班牙'=>array('Spain',self::EUROPE_TAG_GROUP),
        '意大利'=>array('Italy',self::EUROPE_TAG_GROUP),
        '加拿大'=>array('Canada',self::NORTH_TAG_GROUP),
        '哥斯达黎加'=>array('Costa-rican',self::NORTH_TAG_GROUP),
        '多米尼加'=>array('Dominican Republic',self::NORTH_TAG_GROUP),
        '墨西哥'=>array('Mexico',self::NORTH_TAG_GROUP),
        '巴拿马'=>array('Panama',self::NORTH_TAG_GROUP),
        '美国'=>array('United_States',self::NORTH_TAG_GROUP),
        '阿根廷'=>array('Argentina',self::SOUTH_TAG_GROUP),
        '巴西'=>array('Brazil',self::SOUTH_TAG_GROUP),
        '智利'=>array('Chile',self::SOUTH_TAG_GROUP),
        '哥伦比亚'=>array('Colombia',self::SOUTH_TAG_GROUP),
        '秘鲁'=>array('Peru',self::SOUTH_TAG_GROUP),
        '安哥拉'=>array('Angola',self::AFRICA_TAG_GROUP),
        '埃及'=>array('Egypt',self::AFRICA_TAG_GROUP),
        '肯尼亚'=>array('Kenya',self::AFRICA_TAG_GROUP),
        '利比亚'=>array('Libya',self::AFRICA_TAG_GROUP),
        '马达加斯加'=>array('Madagascar',self::AFRICA_TAG_GROUP),
        '摩洛哥'=>array('Morocco',self::AFRICA_TAG_GROUP),
        '南非'=>array('South_Africa',self::AFRICA_TAG_GROUP),
        '津巴布韦'=>array('Zimbabwe',self::AFRICA_TAG_GROUP),
        '澳大利亚'=>array('Australia',self::OCEANIA_TAG_GROUP),
        '新西兰'=>array('New_Zealand',self::OCEANIA_TAG_GROUP)
    );

}