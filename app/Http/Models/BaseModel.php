<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model{

    /**
     * description:deal model more where
     * author:lzy
     * date:2018/11/22
     * @param $tableWhere
     * @param array $data
     * @return mixed
     */
    public function getWhere($tableWhere,$data = array()){
        //if have  field
        if(isset($data['field'])){
            $tableWhere = $tableWhere->select($data['field']);
        }

        //if have where select
        if(isset($data['where'])){
            foreach ($data['where'] as $k=>$v){

                //if where is array
                if(is_array($v)){
                    $tableWhere = $tableWhere->where($k,$v[0],$v[1]);
                }else{
                    $tableWhere = $tableWhere->where($k,$v);
                }
            }
        }

        //if need pageSize
        if(isset($data['pageSize'])){
            $tableWhere = $tableWhere->offset($data['offset'])->limit($data['pageSize']);
        }

        //if order by
        if(isset($data['order_by'])){
            //sort by
            $data['sort_by'] = isset($data['sort_by']) ? $data['sort_by'] : 'DESC';

            $tableWhere = $tableWhere->orderBy($data['order_by'],$data['sort_by']);
        }
        return $tableWhere;
    }


    /**
     * description:get any where message
     * author:lzy
     * date:2018/11/16
     * @param array $data
     * @return array
     */
    public function getWhereMessage($data=array()){
        $result = $this;
        if($data){
            $result = $this->getWhere($result,$data);
        }
        $result = $result->get();
        return $result;
    }

    /**
     * description:get any where message count
     * author:lzy
     * date:2018/11/16
     * @param array $data
     * @return mixed
     */
    public function getWhereCount($data=array()){
        $result = $this;
        if($data){
            $result = $this->getWhere($result,$data);
        }
        $count = $result->count();
        return $count;
    }

    /**
     * description:get any where one message
     * author:lzy
     * date:2018/11/16
     * @param array $data
     * @return array
     */
    public function getWhereOne($data=array()){
        $result = $this;
        if($data){
            $result = $this->getWhere($result,$data);
        }
        $result = $result->first();
        return $result;
    }

    /**
     * description:insert message
     * author:lzy
     * date:2018/11/23
     * @param array $data
     * @return int id
     */
    public function insertMessage($data){
        $data['updated_at'] = $data['created_at'] = date('Y-m-d H:i:s',time());
        $result = $this->insertGetId($data);
        return $result;
    }

    /**
     * description:update message
     * author:lzy
     * date:2018/11/23
     * @param array $where
     * @param array $data
     * @return int id
     */
    public function updateMessage($where,$data){
        $result = $this;
        $data['updated_at'] = date('Y-m-d H:i:s',time());
        if($data){
            $result = $this->getWhere($result,$where);
        }
        $result = $result->update($data);
        return $result;
    }

    /**
     * description:delete any where message
     * author:lzy
     * date:2018/11/16
     * @param array $data
     * @return Category|bool|mixed|null
     * @throws \Exception
     */
    public function deleteMessage($data=array()){
        $result = $this;
        if($data){
            $result = $this->getWhere($result,$data);
        }
        $result = $result->delete();
        return $result;
    }
}