<?php
namespace App\Http\Models;

class Admin extends BaseModel {

    protected $table = 'admin';

    /**
     * description:get one admin user
     * author:lzy
     * date:2018/11/16
     * @param $filed  string
     * @param $val string
     * @return mixed
     */
    public function getAdmin($filed,$val){
        $adminInfo = $this->where($filed,$val)->first();
        return $adminInfo;
    }
}