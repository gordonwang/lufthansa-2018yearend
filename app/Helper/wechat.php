<?php
/**
 * Created by PhpStorm.
 * User: felix.guo1
 * Date: 3/26/18
 * Time: 6:23 PM
 */
function is_wechat() {
    if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false ) {
        return true;
    }
    return false;
}

function use_https() {
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') {
        return true;
    }
    return false;
}

/**
 * describe: add memcache
 * author: lzy
 * date: 10/16/2018
 * @param string $mKey
 * @param string $act
 * @param string $arr
 * @param int $time
 * @return bool
 */
/*function _myMemcache($mKey = '', $act = 'get', $arr = '', $time = 300)
{
    static $memcache_obj;
    if (empty($memcache_obj)) $memcache_obj = new Mymemcache();
    $skey = md5($_SERVER['DOCUMENT_ROOT']);
    if ($act == 'get') return $memcache_obj->get($skey . $mKey);
    if ($act == 'set') return $memcache_obj->set($skey . $mKey, $arr, $time);
    if ($act == 'del') return $memcache_obj->delete($skey . $mKey);
    if ($act == 'add') return $memcache_obj->add($skey . $mKey, $arr);
    return false;
}*/

/**
 * describe: get ip
 * author: lzy
 * date: 10/28/2018
 * @param bool $proxy_override
 * @return string
 */
function get_client_ip_from_ns($proxy_override = false)
{
    if ($proxy_override) {
        /* 优先从代理那获取地址或者 HTTP_CLIENT_IP 没有值 */
        $ip = empty($_SERVER["HTTP_X_FORWARDED_FOR"]) ? (empty($_SERVER["HTTP_CLIENT_IP"]) ? NULL : $_SERVER["HTTP_CLIENT_IP"]) : $_SERVER["HTTP_X_FORWARDED_FOR"];
    } else {
        /* 取 HTTP_CLIENT_IP, 虽然这个值可以被伪造, 但被伪造之后 NS 会把客户端真实的 IP 附加在后面 */
        $ip = empty($_SERVER["HTTP_CLIENT_IP"]) ? NULL : $_SERVER["HTTP_CLIENT_IP"];
    }

    if (empty($ip)) {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    /* 真实的IP在以逗号分隔的最后一个, 当然如果没用代理, 没伪造IP, 就没有逗号分离的IP */
    if ($p = strrpos($ip, ",")) {
        $ip = substr($ip, $p+1);
    }

    return trim($ip);
}