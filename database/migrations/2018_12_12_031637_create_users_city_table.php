<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_city', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mid')->comment('member id');
            $table->string('openid',35)->nullable()->comment('member WeChat openid');
            $table->string('nick_name',50)->nullable()->comment('WeChat nick name');
            $table->string('head')->nullable()->comment('WeChat head');
            $table->text('city')->nullable()->comment('member went city');
            $table->string('img')->nullable()->comment('图片地址');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_city');
    }
}
