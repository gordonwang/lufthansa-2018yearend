<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member', function (Blueprint $table) {
            $table->increments('id')->comment('member id');
            $table->string('openid',35)->nullable()->comment('member WeChat openid');
            $table->string('nick_name',50)->nullable()->comment('WeChat nick name');
            $table->string('head')->nullable()->comment('WeChat head');
            $table->tinyInteger('is_wechat')->default(1)->comment('is wechat');
            $table->text('city')->nullable()->comment('member went city');
            $table->integer('num')->nullable()->comment('member went city number');
            $table->string('img')->nullable()->comment('图片地址');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member');
    }
}
