<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
    'middleware'=>['web']
],function() {
//登录
    Route::get('/admin', 'Index\LoginController@login')->name('login');
    Route::post('/checkLogin', 'Index\LoginController@checkLogin')->name('checkLogin');
    Route::get('/loginOut', 'Index\LoginController@loginOut')->name('loginOut');

//欢迎页
    Route::get('/admin_index', 'Index\IndexController@index')->name('admin_index');
    Route::get('/admin_welcome', 'Index\IndexController@welcome')->name('welcome');

    Route::get('/users_city', 'Index\IndexController@usersCity')->name('users_city');
    Route::get('/users_city_search', 'Index\IndexController@userCitySearch')->name('users_city_search');

    Route::get('/member', 'Index\IndexController@member')->name('member');
    Route::get('/member_search', 'Index\IndexController@memberSearch')->name('member_search');
});
