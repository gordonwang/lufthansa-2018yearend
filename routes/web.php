<?php

/*
|--------------------------------------------------------------------------
| web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
    'middleware'=>['web']
],function() {
    Route::get('/', 'Web\HomeController@index')->name('loading');
    Route::get('/homepage', 'Web\HomeController@homePage')->name('homePage');

//授权
    Route::get('/authorize', 'Web\AuthorizationController@authorizeBack')->name('authorize.back');

    Route::any('/uploadImg', 'Web\UploadController@uploadImg')->name('uploadImg');
    Route::any('/addCity', 'Web\HomeController@addCity')->name('addCity');
    Route::any('/myCity', 'Web\HomeController@myCity')->name('myCity');
});

